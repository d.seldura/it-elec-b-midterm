const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();
const User = require("./model/User");
const Journal = require("./model/Journal");
const bodyParser = require("body-parser");
const Joi = require("@hapi/joi");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

function auth(req, res, next) {
  const token = req.header("auth-token");
  if (!token) return res.status(401).send("Unauthorized");
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.username = verified;
    next();
  } catch (err) {
    res.status(403).send("Forbidden");
  }
}

const createUserSchema = Joi.object({
  username: Joi.string()
    .min(6)
    .required(),
  email: Joi.string()
    .min(6)
    .required()
    .email(),
  password: Joi.string()
    .min(6)
    .required()
});

const loginUserSchema = Joi.object({
  username: Joi.string()
    .min(6)
    .required(),
  password: Joi.string()
    .min(6)
    .required()
});

const journalSchema = Joi.object({
  title: Joi.string()
    .min(6)
    .required(),
  message: Joi.string()
    .min(6)
    .required(),
  imageUrl: Joi.string().allow("")
});

mongoose
  .connect(process.env.MONGODB, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  })
  .then(() => console.log("DB Connected!"))
  .catch((err) => {
    console.log(`DB Connection Error: ${err.message}`);
  });

//middlware
app.use(bodyParser.json());

app.post("/user/create", async (req, res) => {
  console.log(req.body);
  const { error } = createUserSchema.validate(req.body);
  if (error!=undefined) return res.status(400).send(error.details[0].message);

  const emailExist = await User.findOne({ email: req.body.email });
  const usernameExist = await User.findOne({ username: req.body.username });
  if (emailExist || usernameExist)
    return res.status(500).send("Username or Email already in database");

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: hashedPassword
  });
  try {
    const savedUser = await user.save();
    res.status(201).send(savedUser._id);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post("/user/login", async (req, res) => {
  //body validation
  const { error } = loginUserSchema.validate(req.body);
  if (error) return res.status(500).send(error.details[0].message);
  //user exist check
  const user = await User.findOne({ username: req.body.username });
  if (user === null) return res.status(404).send("Username not found");
  else {
    if (req.body.banStatus) return res.status(401).send("Unauthorized");
    const validPass = await bcrypt.compare(req.body.password, user.password);
    //validation
    if (!validPass) return res.status(400).send("Username/Password Invalid");
    else {
      const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, { expiresIn: "1h" });
      res.header("auth-token", token).send(token);
    }
  }
});

app.get("/users", async (req, res) => {
  const users = await User.find();
  if (users.length) return res.status(200).send(users);
  else res.status(500).send("No users in database");
});

app.listen(3000, () => console.log("Listening on port 3000"));

app.get("/users/:input", auth, async (req, res) => {
  const users = await User.find({ username: new RegExp(req.params.input, "gi") });
  if (users.length) return res.status(200).send(users);
  else res.status(500).send(`No users found matching "${req.params.input}"`);
});

app.post("/follow/:userID", auth, async (req, res) => {
  try {
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    const user = await User.findOne({ _id: sourceUser._id });
    await User.findOne({ _id: req.params.userID });
    const duplicate = user.following.find((id) => {
      return id === req.params.userID;
    });
    if (duplicate === undefined) {
      user.following.push(req.params.userID);
      user.save();
      return res.status(200).send(`Following user "${req.params.userID}"`);
    } else throw `You are already following ${req.params.userID}`;
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`User "${req.params.userID}" not found`);
    return res.status(500).send(err);
  }
});

app.post("/unfollow/:userID", auth, async (req, res) => {
  try {
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    const user = await User.findOne({ _id: sourceUser._id });
    await User.findOne({ _id: req.params.userID });
    const duplicate = user.following.find((id) => {
      return id === req.params.userID;
    });
    if (duplicate === undefined) {
      throw `You are not following ${req.params.userID}`;
    } else {
      user.following = user.following.filter((n) => {
        return n != req.params.userID;
      });
      user.save();
      return res.status(200).send(`Unfollowed user "${req.params.userID}"`);
    }
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`User "${req.params.userID}" not found`);
    return res.status(500).send(err);
  }
});

app.get("/following", auth, async (req, res) => {
  try {
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    const user = await User.findOne({ _id: sourceUser._id });
    return res.status(200).send(user.following);
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`User "${req.params.userID}" not found`);
    return res.status(500).send(err);
  }
});

async function getUserPosts(userID){
    var journalsArray = new Array();
    var relevantPosts = await Journal.find({ ownerID: userID });
    relevantPosts.forEach((entry) => {
    journalsArray.push(entry);
  });
  return journalsArray;
}
async function getFollowPosts(userID){
  // var journalsArray = new Array();
  const user = await User.findOne({ _id: userID });
  console.log(user);
    const journalsArray = await Promise.all(user.following.map(async (followID) => {
      return getUserPosts(followID);
    }));
  return journalsArray;
}
app.get("/journals", auth, async (req, res) => {
  try {
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    var journalsArray = new Array();
    console.log("GFP out",await getFollowPosts(sourceUser._id));
    //console.log("GUP out",await getUserPosts(sourceUser._id));
    //Promise.all([getUserPosts(sourceUser._id),getFollowPosts(sourceUser._id)]).then((values)=>console.log("PROM",values));

    var relevantPosts;
    const user = await User.findOne({ _id: sourceUser._id });
      user.following.forEach(async (followID) => {
      relevantPosts = await Journal.find({ ownerID: followID });
      relevantPosts.forEach((entry) => {
        journalsArray.push(entry);
      });
    });

    relevantPosts = await Journal.find({ ownerID: sourceUser._id });
    relevantPosts.forEach((entry) => {
      journalsArray.push(entry);
    });
    

    return res.status(200).send(journalsArray);
  } catch (err) {
    return res.status(500).send(err);
  }
});

app.get("/journal/:postId", auth, async (req, res) => {
  try {
    const post = await Journal.findOne({ _id: req.params.postId });
    return res.status(200).send(post);
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`Journal "${req.params.postId}" not found`);
    return res.status(500).send(err);
  }
});

app.post("/journal", auth, async (req, res) => {
  const { error } = journalSchema.validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
  const journal = new Journal({
    title: req.body.title,
    message: req.body.message,
    imageUrl: req.body.imageUrl === "" ? process.env.DEFAULT_IMAGE : req.body.imageUrl,
    ownerID: sourceUser._id
  });
  try {
    const savedJournal = await journal.save();
    res.status(201).send(savedJournal);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.put("/journal/:postId", auth, async (req, res) => {
  try {
    const post = await Journal.findOne({ _id: req.params.postId });
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    if (sourceUser._id === post.ownerID) {
      const { error } = journalSchema.validate(req.body);
      if (error) return res.status(400).send(error.details[0].message);
      post.title = req.body.title;
      post.message = req.body.message;
      post.imageUrl = req.body.imageUrl === "" ? process.env.DEFAULT_IMAGE : req.body.imageUrl;
      post.save();
      return res.status(200).send(post);
    } else return res.status(403).send(`You do not own ${post.title} by ${post.ownerID}`);
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`Journal "${req.params.postId}" not found`);
    return res.status(500).send(err);
  }
});

app.delete("/journal/:postId", auth, async (req, res) => {
  try {
    const post = await Journal.findOne({ _id: req.params.postId });
    var sourceUser = jwt.verify(req.header("auth-token"), process.env.TOKEN_SECRET);
    if (sourceUser._id === post.ownerID) {
      post.delete();
      return res.status(200).send(`Deleted: ${req.params.postId}`);
    } else return res.status(403).send(`You do not own ${post.title} by ${post.ownerID}`);
  } catch (err) {
    if (err.name === "CastError")
      return res.status(404).send(`Journal "${req.params.postId}" not found`);
    return res.status(500).send(err);
  }
});
